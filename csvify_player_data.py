import unicodecsv as csv
import pickle

players_data = pickle.load(open('player_data.p', 'rb'))

player = players_data[0]
p = player['info']
p_dat = {}
p_dat['ranking'] = p['ranking']
p_dat['name'] = p['name']
p_dat['pos'] = p['position']
p_dat['weight'] = p['weight']
p_dat['height'] = p['height']
p_dat['bday'] = p['birthDate']
p_dat['birth_place'] = p['birthPlace']
p_dat['first_nationality'] = p['firstNationality']
p_dat['country'] = p['country']
p_dat['jersey'] = p['jerseyNum']
p_dat['avail?'] = p['isAvailable']
p_dat['waiver?'] = p['isWaiver']

p_dat['owner'] = p['owner']
p_dat['owned'] = p['owned']
p_dat['ppm'] = p['ppm']

p_dat['injury_type'] = p['injuryType']
p_dat['injury_status'] = p['injuryStatus']
p_dat['join_date'] = p['joinDate']
p_dat['real_pos'] = p['realPosition']
p_dat['real_pos_side'] = p['realPositionSide']
f = p['fixtures'][0]
p_dat['week'] = f['week']
p_dat['team'] = f['homeClub'] if f['isHome'] else f['awayClub']
p_dat['opp_team'] = f['oppTeamKey']
p_dat['minutes'] = f['minutes']
p_dat['total_pts'] = f['totalPoints']
p_dat.update(f['statObj'])
p_dat['is_started'] = f['isStarted']
p_dat['is_completed'] = f['isCompleted']
p_dat['is_home'] = f['isHome']
p_dat['outcome'] = f['outcome']

fieldnames = p_dat.keys()

with open('players_data.csv', 'w') as csvfile:
  writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
  writer.writeheader()

  SPARSE_COLS = {
      'birthPlace':'birth_place',
      'firstNationality':'first_nationality',
      'country':'country',
      'injuryType':'injury_type',
      'injuryStatus':'injury_status'
      }
  for player in players_data:
    p = player['info']
    #print p['name']
    p_dat = {}
    p_dat['ranking'] = p['ranking']
    p_dat['name'] = p['name']
    p_dat['pos'] = p['position']
    p_dat['weight'] = p['weight']
    p_dat['height'] = p['height']
    p_dat['bday'] = p['birthDate']


    for key, val in SPARSE_COLS.iteritems():
      if key in p:
        p_dat[val] = p[key]

    p_dat['jersey'] = p['jerseyNum']
    p_dat['avail?'] = p['isAvailable']
    p_dat['waiver?'] = p['isWaiver']

    p_dat['owner'] = p['owner']
    p_dat['owned'] = p['owned']
    p_dat['ppm'] = p['ppm']

    p_dat['join_date'] = p['joinDate']
    p_dat['real_pos'] = p['realPosition']
    p_dat['real_pos_side'] = p['realPositionSide']
    for f in p['fixtures']:
      p_dat['week'] = f['week']
      p_dat['team'] = f['homeClub'] if f['isHome'] else f['awayClub']
      p_dat['opp_team'] = f['oppTeamKey']
      p_dat['minutes'] = f['minutes']
      p_dat['total_pts'] = f['totalPoints']
      p_dat.update(f['statObj'])
      p_dat['is_started'] = f['isStarted']
      p_dat['is_completed'] = f['isCompleted']
      p_dat['is_home'] = f['isHome']
      p_dat['outcome'] = f['outcome']

      writer.writerow(p_dat)

