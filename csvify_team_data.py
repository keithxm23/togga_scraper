import csv
import dill
import pickle
from sets import Set

teams_data = pickle.load(open('team_data.p', 'rb'))

fieldnames = Set([])
for team_name, team_data in teams_data.iteritems():
  for week, week_data in team_data.iteritems():
    fieldnames = fieldnames.union(week_data.keys())


with open('teams_data.csv', 'w') as csvfile:
  csv_headers = ['team','week'] + list(fieldnames)
  writer = csv.DictWriter(csvfile, fieldnames=csv_headers)
  writer.writeheader()
  for team_name, team_data in teams_data.iteritems():
    for week, week_data in team_data.iteritems():
      data = week_data.copy()
      data.update({'team': team_name, 'week': week})
      writer.writerow(data)


