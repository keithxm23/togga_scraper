import dill
#import cPickle as pickle
import pickle
import json
from collections import defaultdict
import operator

POSITIONS = [u'Full Back', u'Second Striker', u'Attacking Midfielder',
  u'Central Defender', u'Unknown', u'Striker', u'Wing Back',
  u'Defensive Midfielder', u'Central Midfielder', u'Goalkeeper', u'Winger']

TOP_SIX = ['CHE', 'ARS', 'LIV', 'MCI', 'MUN', 'TOT']

def parse_player_data():
  player_data_file = 'player_data.p'
  player_data = pickle.load(open(player_data_file, 'rb'))

  team_data = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))

  def record_team_stat(for_team, opp_team, stat, val):
    for_team[stat] += val
    opp_team[stat+"_c"] += val
    return

  for player in player_data:
    p = player['info']

    for f in p['fixtures']:

      total_pts = f['totalPoints'] if f['totalPoints'] else 0

      if f['isHome']:
        team = f['homeClub']
        opp_team = f['awayClub']
        team_data[opp_team][f['week']]['when_away_pts_c'] += total_pts
        team_data[opp_team][f['week']]['is_home'] = False
        team_data[team][f['week']]['is_home'] = True
      else:
        opp_team = f['homeClub']
        team = f['awayClub']
        team_data[opp_team][f['week']]['when_home_pts_c'] += total_pts
        team_data[opp_team][f['week']]['is_home'] = True
        team_data[team][f['week']]['is_home'] = False

      team_data[team][f['week']]['agnst_top6'] = opp_team in TOP_SIX
      team_data[opp_team][f['week']]['agnst_top6'] = team in TOP_SIX

      for_team_fix = team_data[team][f['week']]
      opp_team_fix = team_data[opp_team][f['week']]

      record_team_stat(for_team_fix, opp_team_fix, 'points', total_pts)
      record_team_stat(for_team_fix, opp_team_fix, 'assists', f['statObj']['A'])
      record_team_stat(for_team_fix, opp_team_fix, 'goals', f['statObj']['G'])
      record_team_stat(for_team_fix, opp_team_fix, 'pens_saved', f['statObj']['PS'])
      record_team_stat(for_team_fix, opp_team_fix, 'aerials', f['statObj']['AER'])
      record_team_stat(for_team_fix, opp_team_fix, 'chances_created', f['statObj']['CC'])
      record_team_stat(for_team_fix, opp_team_fix, 'tackles_won', f['statObj']['TW'])
      record_team_stat(for_team_fix, opp_team_fix, 'yellows', f['statObj']['YC'])
      record_team_stat(for_team_fix, opp_team_fix, 'successful_take_ons', f['statObj']['STO'])
      record_team_stat(for_team_fix, opp_team_fix, 'saves', f['statObj']['SV'])
      record_team_stat(for_team_fix, opp_team_fix, 'own_goals', f['statObj']['OG'])
      record_team_stat(for_team_fix, opp_team_fix, 'shots_on_target', f['statObj']['SOT'])
      record_team_stat(for_team_fix, opp_team_fix, 'reds', f['statObj']['RC'])
      record_team_stat(for_team_fix, opp_team_fix, 'clean_sheets', f['statObj']['CS'])
      record_team_stat(for_team_fix, opp_team_fix, 'dispossed', f['statObj']['DIS'])
      record_team_stat(for_team_fix, opp_team_fix, 'interceptions', f['statObj']['INT'])
      record_team_stat(for_team_fix, opp_team_fix, 'clearances', f['statObj']['CLR'])
      record_team_stat(for_team_fix, opp_team_fix, 'successful_crosses', f['statObj']['SCR'])

      #Todo, handle owngoals
      #OGs are not recorded as goals conceded at the moment, nor as goals scored for

      #TODO, filter None team

      real_pos_name = p['realPosition'].lower().replace(' ','_')+'_pts'
      record_team_stat(for_team_fix, opp_team_fix, real_pos_name, total_pts)

      short_pos_name = p['shortPosition'].lower().replace(' ','_')+'_pts'
      record_team_stat(for_team_fix, opp_team_fix, short_pos_name, total_pts)

  pickle.dump(team_data, open("team_data.p", "wb"))


parse_player_data()
#team_data = pickle.load(open('team_data.p', 'rb'))
#player_data = pickle.load(open('player_data.p', 'rb'))

