import requests
import cPickle as pickle
import json
import time
import random
import datetime
import getpass
from collections import defaultdict
import dateutil.parser

LOGIN_URL = 'https://api.playtogga.com/login.php'
GET_STANDINGS_URL = 'https://api.playtogga.com/leagues/getstandings.php'
GET_PLAYERLIST_URL = 'https://api.playtogga.com/players/getplayerlist.php'
GET_PLAYERINFO_URL = 'https://api.playtogga.com/players/getplayerinfo.php'
GET_TRANSACTIONS_URL = 'https://api.playtogga.com/leagues/gettransactions.php'
GET_STANDINGS_URL = 'https://api.playtogga.com/leagues/getstandings.php'

email = raw_input('Enter your togga account email:')
pw = getpass.getpass('Enter your togga account password:')
headers = {
  'User-Agent': 'Mozilla/5.0',
  'Accept': 'application/json',
  'From': email
}

def authenticate():
  data = {
    'app':'draft',
    'email':email,
    'password':pw
  }
  r = requests.post(LOGIN_URL, data=data, headers=headers)
  print(r.text)
  return r.cookies


def get_player_list(remote = False):
  if remote:
    r = requests.post(GET_PLAYERLIST_URL, headers=headers, cookies=cookies,
        data={'league_id':'57addc85cfd4dc1000d9157a'})
    pickle.dump(json.loads(r.text), open("player_list.p", "wb"))
    return json.loads(r.text)
  else:
    return pickle.load(open("player_list.p", "rb"))


def get_player_info(player_id):
  r = requests.post(GET_PLAYERINFO_URL, headers=headers, cookies=cookies,
      data={'player_id':player_id})
  if r.status_code == requests.codes.ok:
    return json.loads(r.text)
  else:
    print(u'Retrying...')
    return get_player_info(player_id)

def get_all_players_info(remote = False):
  if remote:
    player_list = get_player_list(remote = remote)
    player_data = []

    for p in player_list['info']:
      print(u"Scraping player {0}".format(p['name']))
      player_info = get_player_info(p['playerId'])
      player_info['info']['isAvailable'] = p['isAvailable']
      player_info['info']['isWaiver'] = p['isWaiver']
      player_info['info']['owner'] = p['ownerName']
      player_info['info']['owned'] = p['ownedCount']
      player_info['info']['ppm'] = p['ppm']
      player_data.append(player_info)
      sleep_time = random.randint(1,10)
      #print("Sleeping for {0} second(s)..".format(sleep_time,))
      #time.sleep(sleep_time)

    pickle.dump(player_data, open("player_data"+str(datetime.datetime.now()).replace(' ','_')+".p", "wb"))
  else:
    return pickle.load(open('player_data.p', 'rb'))

def get_standings():
  r = requests.post(GET_STANDINGS_URL, headers=headers, cookies=cookies,
      data={'league_id':'57addc85cfd4dc1000d9157a'})
  return json.loads(r.text)

def get_transactions():
  r = requests.post(GET_TRANSACTIONS_URL, headers=headers, cookies=cookies,
      data={
        'league_id':'57addc85cfd4dc1000d9157a',
        'transaction_type':0
        })
  return json.loads(r.text)

cookies = authenticate()
#player_data = get_all_players_info()
