from togga_scraper import *

def investigate_waiver_locks():
  DAYS_HELD_THRESHOLD = 5
  team_map = {t['teamId']:t['owner'] for t in get_standings()['info']}
  trx = get_transactions()
  teams = defaultdict(dict)

  transfers = [t for t in trx['info'] if t['type'] in ['Add', 'Drop']]
  for t in transfers:
    _parse_added_players(t, teams, team_map, DAYS_HELD_THRESHOLD)
    _parse_dropped_players(t, teams, team_map, DAYS_HELD_THRESHOLD)


def _parse_added_players(trx, teams, team_map, DAYS_HELD_THRESHOLD):
  for p in trx['playersAdded']:
    plyr_name = p['personName']['common']
    teams[trx['teamId']][plyr_name] = dateutil.parser.parse(trx['createdAt'])
    #print(u"{0} picked up {1}").format(team_map[trx['teamId']], plyr_name)

def _parse_dropped_players(trx, teams, team_map, DAYS_HELD_THRESHOLD):
  for p in trx['playersDropped']:
    plyr_name = p['personName']['common']
    if plyr_name in teams[trx['teamId']]:
      days_held = (
          dateutil.parser.parse(trx['createdAt']) - teams[trx['teamId']][plyr_name]
          ).days
      picked_on = teams[trx['teamId']][plyr_name].strftime('%a, %d %b')
      dropped_on = dateutil.parser.parse(trx['createdAt']).strftime('%a, %d %b')

      #print(u"{0} dropped {1} after {2} days. Picked: {3}, Dropped: {4}").format(
      #    team_map[trx['teamId']], plyr_name, days_held, picked_on, dropped_on)

      if days_held < DAYS_HELD_THRESHOLD:
        print(u"{0} dropped {1} after {2} days. Picked: {3}, Dropped: {4}\n").format(
            team_map[trx['teamId']], plyr_name, days_held, picked_on, dropped_on)

      teams[trx['teamId']].pop(plyr_name)
    else:
      pass
      #player owned by draft or trade
      #print(u"{0} dropped TRADED/DRAFTED PLAYER {1}").format(team_map[trx['teamId']], plyr_name)

investigate_waiver_locks()
